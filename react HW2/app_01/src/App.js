import React from 'react';
import Header from './components/Header';
import Footer from './components/Footer';
import Items from './components/Items';

class App extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      orders:[],
      items: [
        {
          id:1,
          title:'Стул серый',
          img:"images.jpg",
          desc:"lorem ipsum dolor sit amet",
          category:"chair",
          price:"49.99"
        },
        {
          id:2,
          title:'Стул серый',
          img:"chair-grey.jpg",
          desc:"lorem ipsum dolor sit amet",
          category:"chair",
          price:"49.99"
        },
        {
          id:3,
          title:'Стул серый',
          img:"images.jpg",
          desc:"lorem ipsum dolor sit amet",
          category:"chair",
          price:"49.99"
        },
        {
          id:4,
          title:'Стул серый',
          img:"chair-grey.jpg",
          desc:"lorem ipsum dolor sit amet",
          category:"chair",
          price:"49.99"
        },
        {
          id:5,
          title:'Стул серый',
          img:"chair-grey.jpg",
          desc:"lorem ipsum dolor sit amet",
          category:"chair",
          price:"49.99"
        },
        {
          id:6,
          title:'Стул серый',
          img:"images.jpg",
          desc:"lorem ipsum dolor sit amet",
          category:"chair",
          price:"49.99"
        },
        {
          id:7,
          title:'Стул серый',
          img:"chair-grey.jpg",
          desc:"lorem ipsum dolor sit amet",
          category:"chair",
          price:"49.99"
        },
        {
          id:8,
          title:'Стул серый',
          img:"images.jpg",
          desc:"lorem ipsum dolor sit amet",
          category:"chair",
          price:"49.99"
        },
        {
          id:9,
          title:'Стул серый',
          img:"chair-grey.jpg",
          desc:"lorem ipsum dolor sit amet",
          category:"chair",
          price:"49.99"
        },
        {
          id:10,
          title:'Стул серый',
          img:"images.jpg",
          desc:"lorem ipsum dolor sit amet",
          category:"chair",
          price:"49.99"
        },

      ]
    }


    this.addToOreder = this.addToOreder.bind(this)
  }
  render(){
    return (
      <div className="wrapper">
        <Header orders={this.state.orders}/>
        <Items items={this.state.items} onAdd={this.addToOreder}/>
        <Footer/>
       </div>
    );
  }

  addToOreder(item){
    let colorBtn = document.querySelector(".add-to-cart")
    let isInArray = false
    this.state.orders.forEach(el=>{
      if(el.id === item.id){
        isInArray= true
        colorBtn.style.color = 'green'
      }
    })
    if(!isInArray){
      this.setState({orders:[...this.state.orders, item] })
    }

    
    
  }
  
  }
 
export default App;
